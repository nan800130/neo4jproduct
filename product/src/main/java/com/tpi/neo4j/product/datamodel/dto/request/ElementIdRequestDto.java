package com.tpi.neo4j.product.datamodel.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

// Dto object with basic validation
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class ElementIdRequestDto {
        @NotBlank
        private String elementId;

}