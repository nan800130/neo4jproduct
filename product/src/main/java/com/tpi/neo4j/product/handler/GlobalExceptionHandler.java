package com.tpi.neo4j.product.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.tpi.neo4j.product.dto.ErrorResponse;
import com.tpi.neo4j.product.exception.AuthHttpException;
import com.tpi.neo4j.product.exception.GraphException;
import com.tpi.neo4j.product.exception.HttpException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(HttpException.class)
  public ResponseEntity<ErrorResponse> handleHttpException(HttpException e) {
    log.error("Error Handler:HttpException", e);
    return new ResponseEntity<>(new ErrorResponse("ERR_001", e.getMessage()), HttpStatus.OK);
  }

  @ExceptionHandler(GraphException.class)
  public ResponseEntity<ErrorResponse> handleGraphException(GraphException e) {
    log.error("Error Handler:GraphException", e);
    return new ResponseEntity<>(new ErrorResponse("ERR_002", e.getMessage()), HttpStatus.OK);
  }
  
  @ExceptionHandler(AuthHttpException.class)
  public ResponseEntity<ErrorResponse> handleAuthHttpException(AuthHttpException e) {
    log.error("Error Handler:AuthHttpException", e);
    return new ResponseEntity<>(new ErrorResponse("ERR_003", e.getMessage()), HttpStatus.FORBIDDEN);
  }
  
  @ExceptionHandler(AuthenticationException.class)
  public ResponseEntity<ErrorResponse> handleAuthenticationException(AuthenticationException e) {
    log.error("Error Handler:AuthenticationException", e);
    return new ResponseEntity<>(new ErrorResponse("ERR_003", "登入失敗，用戶帳號或密碼錯誤。"), HttpStatus.FORBIDDEN);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ErrorResponse> handleException(Exception ex) {
    log.error("Error Handler:Exception:" + ex.getMessage(), ex);
    return new ResponseEntity<>(
        new ErrorResponse("ERR_999", "系統發生錯誤。"), HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
