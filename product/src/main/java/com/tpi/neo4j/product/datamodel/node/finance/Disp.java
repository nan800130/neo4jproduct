package com.tpi.neo4j.product.datamodel.node.finance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.neo4j.core.schema.Node;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Node("Disp")
public class Disp extends com.tpi.neo4j.product.datamodel.node.Node {
    @JsonProperty("disp_id")
    private Integer disp_id;

    @JsonProperty("account_id")
    private Integer account_id;

    @JsonProperty("client_id")
    private Integer client_id;

    @JsonProperty("type")
    private String type;
}
