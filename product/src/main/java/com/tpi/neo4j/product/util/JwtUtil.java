package com.tpi.neo4j.product.util;

import java.util.Date;
import java.util.UUID;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class JwtUtil {

//  private JwtUtil() {
//    throw new IllegalStateException("JwtUtil class");
//  }

  // 有效期为30分鐘
  public static final Long JWT_TTL = 30 * 60 * 1000L;
  // 设置密鑰明文
  public static final String JWT_KEY = "1qaz@WSX3edc$RFV5tgb^YHN7ujm*IK<9ol.";

  public static String getUuid() {
    return UUID.randomUUID().toString().replaceAll("-", "");
  }

  /**
   * 生成jwt
   *
   * @param subject: token中要存放的数据(Json格式)
   * @return: java.lang.String
   */
  public static String createJwt(String subject) {
    // 设置过期时间
    JwtBuilder builder = getJwtBuilder(subject, null, getUuid());
    return builder.compact();
  }

  /**
   * 生成jwt
   *
   * @param subject: token中要存放的数据（Json）格式
   * @param ttlMillis:token超时时间
   * @return: java.lang.String
   */
  public static String createJwt(String subject, Long ttlMillis) {
    // 设置过期时间
    JwtBuilder builder = getJwtBuilder(subject, ttlMillis, getUuid());
    return builder.compact();
  }

  private static JwtBuilder getJwtBuilder(String subject, Long ttlMillis, String uuid) {
    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
    SecretKey secretKey = generalkey();
    long nowMillis = System.currentTimeMillis();
    Date now = new Date(nowMillis);
    if (ttlMillis == null) {
      ttlMillis = JwtUtil.JWT_TTL;
    }
    Date expDate = new Date(nowMillis + ttlMillis);
    return Jwts.builder()
        // 唯一Id
        .setId(uuid)
        // 主体，可以是JSON数据
        .setSubject(subject)
        // 签发者
        .setIssuer("tpitest")
        // 签发时间
        .setIssuedAt(now)
        // 使用HS256对称加密算法签名，第二个参数为密钥
        .signWith(secretKey, signatureAlgorithm)
        .setExpiration(expDate);
  }

  /**
   * 创建token
   *
   * @param id:
   * @param subject:
   * @param ttlMillis:
   * @return:
   */
  public static String createJwt(String id, String subject, Long ttlMillis) {
    return getJwtBuilder(subject, ttlMillis, id).compact();
  }

  /** 生成加密后的密钥 secretkey */
  public static SecretKey generalkey() {
    return new SecretKeySpec(JWT_KEY.getBytes(), "HmacSHA256");
  }

  /** 解析 jwt */
  public static Claims parseJwt(String jwt) {
    SecretKey secretKey = generalkey();
    return Jwts.parserBuilder().setSigningKey(secretKey).build().parseClaimsJws(jwt).getBody();
  }
}
