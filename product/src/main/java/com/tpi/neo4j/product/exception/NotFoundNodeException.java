package com.tpi.neo4j.product.exception;

public class NotFoundNodeException extends FetchNodeException {
    public NotFoundNodeException(String message) {
        super(message);
    }
}
