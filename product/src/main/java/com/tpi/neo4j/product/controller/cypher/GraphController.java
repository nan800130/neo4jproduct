package com.tpi.neo4j.product.controller.cypher;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tpi.neo4j.product.datamodel.dto.request.GraphExpandRequestDto;
import com.tpi.neo4j.product.util.CreateGojs;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/graph")
public class GraphController {
	
  @Autowired private CreateGojs createGojs;

  @PostMapping(value = "/expand", produces = MediaType.APPLICATION_JSON_VALUE)
  public String graphExpandQuery(@Valid @RequestBody GraphExpandRequestDto graphExpandRequestDto)
      throws IOException {

    String expandGraphQuery =
        "MATCH (n)-[r]-(m) where (toLower(type(r)) = toLower('"
            + graphExpandRequestDto.getType()
            + "')) and elementid(n)='"
            + graphExpandRequestDto.getElementId()
            + "' RETURN n,r,m ";
    return createGojs.execute(expandGraphQuery);
  }
}
