package com.tpi.neo4j.product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tpi.neo4j.product.datamodel.dto.request.LoginRequestDto;
import com.tpi.neo4j.product.datamodel.dto.request.SignupRequestDto;
import com.tpi.neo4j.product.dto.BaseResponse;
import com.tpi.neo4j.product.dto.TokenResponse;
import com.tpi.neo4j.product.exception.AuthHttpException;
import com.tpi.neo4j.product.service.AuthService;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/auth")
public class AuthController {

  @Autowired private InMemoryUserDetailsManager inMemoryUserDetailsManager;

  @Autowired private AuthService authService;

  @PostMapping("/register")
  @Operation(security = {})
  public ResponseEntity<BaseResponse> register(@Valid @RequestBody SignupRequestDto signupRequestDto) {

    // verify username is already exist, then return duplicate username message
	if(inMemoryUserDetailsManager.userExists(signupRequestDto.getUsername())) {
		throw new AuthHttpException("username已註冊過，請勿重複註冊");
	}
	  
    UserDetails newUser =
        User.withUsername(signupRequestDto.getUsername())
            .password(signupRequestDto.getPassword())
            .build();

    log.info("before add user into inMemoryUserDetailsManager : {}", newUser.getUsername());

    // do the register new user process with "in-memory"
    inMemoryUserDetailsManager.createUser(newUser);

    log.info(
        "after add user into inMemoryUserDetailsManager : {}",
        inMemoryUserDetailsManager.userExists(newUser.getUsername()));

    return ResponseEntity.ok(new BaseResponse("200", "註冊成功"));
  }

  @PostMapping("/login")
  @Operation(security = {})
  public ResponseEntity<TokenResponse> login(@Valid @RequestBody LoginRequestDto loginRequestDto) throws Exception {

    return ResponseEntity.ok(new TokenResponse(authService.login(loginRequestDto), ""));
  }

  @PostMapping("/logout")
  public ResponseEntity<BaseResponse> logout(HttpServletRequest request, HttpServletResponse response) {

    // 1.取token 2.auth登出 3.刪掉快取 4.扣掉登錄人數
    authService.logout(request, response);

    return ResponseEntity.ok(new BaseResponse("200", "登出成功"));
  }
}
