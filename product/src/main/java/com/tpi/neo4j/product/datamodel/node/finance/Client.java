package com.tpi.neo4j.product.datamodel.node.finance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.neo4j.core.schema.Node;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Node("Client")
public class Client extends com.tpi.neo4j.product.datamodel.node.Node {

    @JsonProperty("birth_number")
    private Integer birth_number;

    @JsonProperty("district_id")
    private Integer district_id;

    @JsonProperty("client_id")
    private Integer client_id;

}
