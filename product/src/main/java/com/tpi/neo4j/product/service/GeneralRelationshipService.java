package com.tpi.neo4j.product.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tpi.neo4j.product.datamodel.dto.request.Top5RelationshipEntityDto;
import com.tpi.neo4j.product.repository.GeneralRelationshipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class GeneralRelationshipService {

    @Autowired
    private GeneralRelationshipRepository generalRelationshipRepository;

    public Map<String,List<String>> parseDistinctRelationshipTypes(String str){

        JsonObject jsonObject = JsonParser.parseString(str).getAsJsonObject();
        String key = jsonObject.get("results").getAsJsonArray().get(0).getAsJsonObject().get("columns").getAsJsonArray().get(0).getAsString();
        JsonArray data = jsonObject.get("results").getAsJsonArray().get(0).getAsJsonObject().get("data").getAsJsonArray();

        List<String> distinctRelationshipTypesList  = new ArrayList<>();

        for(JsonElement element : data){
            String elementValue = element.getAsJsonObject().get("row").getAsJsonArray().get(0).getAsString();
            distinctRelationshipTypesList.add(elementValue);
        }

        Map<String,List<String>> distinctRelationshipTypes = new HashMap<>();
        distinctRelationshipTypes.put(key, distinctRelationshipTypesList);

        return distinctRelationshipTypes;
    }

    public Map<String,List<String>> connectedRelationships(String elementId){
        Map<String,List<String>> connectedRelationships = new HashMap<>();
        connectedRelationships.put("connectedRelationships",generalRelationshipRepository.connectedRelationships(elementId));
        return connectedRelationships;
    }

    public Map<String,List<Top5RelationshipEntityDto>> top5connectedRelationships(String elementId){
        Map<String, List<Top5RelationshipEntityDto>> top5connected = new HashMap<>();
        top5connected.put("top5",generalRelationshipRepository.top5ConnectedRelationships(elementId));
        return top5connected;
    }

    public Map<String,List<String>> containsString(String elementId, String searchKeyStr){
        Map<String,List<String>> keySearch = new HashMap<>();
        keySearch.put("keySearch",generalRelationshipRepository.containsString(elementId,searchKeyStr));
        return keySearch;
    }
}