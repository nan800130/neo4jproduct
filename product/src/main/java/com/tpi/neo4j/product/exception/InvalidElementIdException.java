package com.tpi.neo4j.product.exception;

public class InvalidElementIdException extends GraphException {
    public InvalidElementIdException(String message) {
        super(message);
    }
}