package com.tpi.neo4j.product.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse implements Serializable {

  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  private String errorCode;

  private String errorMessage;
}
