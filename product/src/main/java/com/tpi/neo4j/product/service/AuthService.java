package com.tpi.neo4j.product.service;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;

import com.tpi.neo4j.product.datamodel.dto.request.LoginRequestDto;
import com.tpi.neo4j.product.exception.HttpException;
import com.tpi.neo4j.product.util.JwtUtil;
import com.tpi.neo4j.product.util.KeyPairUtil;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@CacheConfig(cacheNames = "LoginToken")
@Service
public class AuthService {

  @Autowired private AuthenticationManager authenticationManager;

  @Autowired private RedisService redisService;

  @Autowired private KeyPairUtil keyPairUtil;
  
  @Cacheable(value = RedisService.STR_LOGIN_TOKEN, key = "#loginRequestDto.username")
  public String login(LoginRequestDto loginRequestDto) throws Exception {

    String username = loginRequestDto.getUsername();
    keyPairUtil.validCertificate();

    redisService.checkDuplicateLogin(loginRequestDto.getUsername());
    
    // 取得認證
    UsernamePasswordAuthenticationToken token =
        new UsernamePasswordAuthenticationToken(username, loginRequestDto.getPassword());

    Authentication authenticate = authenticationManager.authenticate(token);

    // 認證没通過
    if (Objects.isNull(authenticate)) {
      throw new HttpException("登錄失敗");
    }

    redisService.checkAuthLimit();
    
    return JwtUtil.createJwt(username);
  }

  /**
   * @param token
   */
  public void logout(HttpServletRequest request, HttpServletResponse response) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
        new SecurityContextLogoutHandler().logout(request, response, auth);
        String loginUser = authentication.getName();
        if (StringUtils.isNotBlank(loginUser)) {
        	
        	redisService.removeToken(loginUser);
        }
    }
  }
}
