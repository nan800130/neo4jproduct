package com.tpi.neo4j.product.datamodel.dto.gojs;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class Linked implements Serializable {
  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  private String cypheString;

  private List<GoJS> items;
}
