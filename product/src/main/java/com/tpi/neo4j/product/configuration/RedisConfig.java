package com.tpi.neo4j.product.configuration;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.tpi.neo4j.product.service.RedisService;

@EnableCaching
@Configuration
public class RedisConfig {

  @Bean
  public RedisCacheManager cacheManager(RedisConnectionFactory connectionFactory) {
    RedisCacheConfiguration config =
        RedisCacheConfiguration.defaultCacheConfig()
            .entryTtl(Duration.ofHours(1)) // 默認逾期时間一小時
            .serializeKeysWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new StringRedisSerializer()))
            .serializeValuesWith(
                RedisSerializationContext.SerializationPair.fromSerializer(
                    new GenericJackson2JsonRedisSerializer()));

    Map<String, RedisCacheConfiguration> configMap = new HashMap<>();
    configMap.put(
        RedisService.STR_LOGIN_TOKEN, config.entryTtl(Duration.ofMinutes(30))); // token逾期时間30分鐘
    configMap.put(RedisService.STR_SYSTEM_COUNT, config); // 使用默认配置
    configMap.put(RedisService.STR_SELECT_DB, config); // 使用默认配置

    return RedisCacheManager.builder(connectionFactory)
        .cacheDefaults(config)
        .withInitialCacheConfigurations(configMap)
        .build();
  }

  @Bean
  public RedisTemplate<String, Object> redisTemplate(
      RedisConnectionFactory redisConnectionFactory) {
    RedisTemplate<String, Object> template = new RedisTemplate<>();
    template.setConnectionFactory(redisConnectionFactory);
    // template.setConnectionFactory(lettuceConnectionFactory());
    template.setDefaultSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
    template.setKeySerializer(new StringRedisSerializer());
    template.setEnableTransactionSupport(true);
    template.afterPropertiesSet();
    
    return template;
  }
}
