package com.tpi.neo4j.product.datamodel.node.finance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.neo4j.core.schema.Node;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Node("District")
public class District extends com.tpi.neo4j.product.datamodel.node.Node {
    @JsonProperty("district_id")
    private Integer district_id;
    @JsonProperty("A1")
    private Integer A1;
    @JsonProperty("A2")
    private String A2;
    @JsonProperty("A3")
    private String A3;
    @JsonProperty("A4")
    private Integer A4;
    @JsonProperty("A5")
    private Integer A5;
    @JsonProperty("A6")
    private Integer A6;
    @JsonProperty("A7")
    private Integer A7;
    @JsonProperty("A8")
    private Integer A8;
    @JsonProperty("A9")
    private Integer A9;
    @JsonProperty("A10")
    private Double A10;
    @JsonProperty("A11")
    private Integer A11;
    @JsonProperty("A12")
    private Double A12;
    @JsonProperty("A13")
    private Double A13;
    @JsonProperty("A14")
    private Integer A14;
    @JsonProperty("A15")
    private Integer A15;
    @JsonProperty("A16")
    private Integer A16;

}
