package com.tpi.neo4j.product.configuration;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.tpi.neo4j.product.datamodel.dto.db.Neo4jDbSetting;
import com.tpi.neo4j.product.exception.HttpException;

import lombok.Data;

@Data
@Component
@PropertySource("classpath:neo4jDb.properties")
@ConfigurationProperties(prefix = "neo4j")
public class Neo4jDbConfig {

  private List<Neo4jDbSetting> dbs;

  /**
   * 取得所有設定的資料庫名稱
   *
   * @return
   */
  public List<String> getDatabaseList() {
    if (CollectionUtils.isEmpty(dbs)) {
      return Collections.emptyList();
    }

    return dbs.stream().map(Neo4jDbSetting::getDatabase).collect(Collectors.toList());
  }

  /**
   * 取得該資料庫連線資訊
   *
   * @param databaseName 資料庫名稱
   * @return
   * @throws HttpException
   */
  public Neo4jDbSetting getDbSetting(String databaseName) throws HttpException {

    if (CollectionUtils.isEmpty(dbs)) {
      throw new HttpException("找不到Neo4j設定檔");
    }

    if (StringUtils.isBlank(databaseName)) {
      databaseName = dbs.get(0).getDatabase();
    }

    for (Neo4jDbSetting setting : dbs) {
      if (StringUtils.equals(databaseName, setting.getDatabase())) {
        return setting;
      }
    }
    throw new HttpException("找不到Neo4j設定檔:" + databaseName);
  }
}
