package com.tpi.neo4j.product.datamodel.node.finance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.neo4j.core.schema.Node;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Node("Transaction")
public class Transaction extends com.tpi.neo4j.product.datamodel.node.Node {

    @JsonProperty("account_id")
    private Integer account_id;
    @JsonProperty("trans_id")
    private Integer trans_id;
    @JsonProperty("date")
    private Integer date;
    @JsonProperty("bank")
    private String bank;
    @JsonProperty("k_symbol")
    private String k_symbol;
    @JsonProperty("type")
    private String type;
    @JsonProperty("operation")
    private String operation;
    @JsonProperty("account")
    private String account;
    @JsonProperty("amount")
    private Double amount;
    @JsonProperty("balance")
    private Double balance;




}
