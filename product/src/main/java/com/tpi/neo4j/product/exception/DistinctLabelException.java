package com.tpi.neo4j.product.exception;

public class DistinctLabelException extends GraphException {
    public DistinctLabelException(String message) {
        super(message);
    }
}