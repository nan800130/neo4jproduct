package com.tpi.neo4j.product.datamodel.node;


import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@org.springframework.data.neo4j.core.schema.Node
public class Node {
    @Id
    @GeneratedValue
    private Long id;

}
