package com.tpi.neo4j.product.exception;

public class GraphException extends Exception {
    String errorMessage;
    Integer errorCode;

    public void setErrorMessageAndErrorCode(String errMag, Integer errCode){
        this.errorMessage = errMag;
        this.errorCode = errCode;
    }
    public GraphException(String message) {
        super(message);
    }
}
