package com.tpi.neo4j.product.exception;

public class FetchNodeException extends GraphException {
    public FetchNodeException (String message) {
        super(message);
    }
}