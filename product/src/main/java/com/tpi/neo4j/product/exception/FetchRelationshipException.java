package com.tpi.neo4j.product.exception;

public class FetchRelationshipException extends GraphException {
    public FetchRelationshipException() {
        super("FetchRelationshipException occurs");
    }
    public FetchRelationshipException(String message) {
        super(message);
    }
}