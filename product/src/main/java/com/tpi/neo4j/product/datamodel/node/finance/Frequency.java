package com.tpi.neo4j.product.datamodel.node.finance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.neo4j.core.schema.Node;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Node("Frequency")
public class Frequency extends com.tpi.neo4j.product.datamodel.node.Node {

    @JsonProperty("frequency")
    private String freq;

    public String getFrequency() {
        return this.freq;
    }
}
