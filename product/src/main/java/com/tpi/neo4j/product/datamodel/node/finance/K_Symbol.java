package com.tpi.neo4j.product.datamodel.node.finance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.neo4j.core.schema.Node;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Node("k_symbol")
public class K_Symbol extends com.tpi.neo4j.product.datamodel.node.Node {
    @JsonProperty("symbol")
    private String symbol;

}
