package com.tpi.neo4j.product.exception;

public class NotAnyMatchException extends GraphException {
    public NotAnyMatchException() {
        super("NotAnyMatchException occurs");
    }
    public NotAnyMatchException(String message) {
        super(message);
    }

}