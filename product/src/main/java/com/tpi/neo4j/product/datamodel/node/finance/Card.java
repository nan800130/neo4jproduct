package com.tpi.neo4j.product.datamodel.node.finance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.neo4j.core.schema.Node;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Node("Card")
public class Card extends com.tpi.neo4j.product.datamodel.node.Node {

    @JsonProperty("card_id")
    private Integer card_id;
    @JsonProperty("issued_date")
    private Integer issued_date;
    @JsonProperty("type")
    private String type;
}
