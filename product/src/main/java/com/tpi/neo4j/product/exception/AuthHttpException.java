package com.tpi.neo4j.product.exception;

import org.springframework.security.core.AuthenticationException;

public class AuthHttpException extends AuthenticationException {
  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  public AuthHttpException(String message) {
    super(message);
  }
}
