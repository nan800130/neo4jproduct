package com.tpi.neo4j.product.datamodel.dto.response;

import java.util.HashMap;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDto {

  /** 狀態碼 */
  private String code;

  /** 後端訊息 */
  private String message;

  /** 資料 */
  private Object data;

  public static ResponseDto ok() {

    return new ResponseDto("200", "執行成功", new HashMap<>());
  }

  public static ResponseDto ok(Object data) {

    return new ResponseDto("200", "執行成功", data);
  }

  public static ResponseDto ok(String message, Object data) {
    return new ResponseDto("200", message, data);
  }

  public static ResponseDto failare() {
    return new ResponseDto("500", "執行失敗", new HashMap<>());
  }

  public static ResponseDto failare(String message) {
    return new ResponseDto("500", message, new HashMap<>());
  }
  
  public static ResponseDto failare(String code, String message) {
	    return new ResponseDto(code, message, new HashMap<>());
	  }
}
