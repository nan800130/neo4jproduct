package com.tpi.neo4j.product.exception;

public class InvalidNodeLabelException extends GraphException {
  public InvalidNodeLabelException(String message) {
    super(message);
  }
}
