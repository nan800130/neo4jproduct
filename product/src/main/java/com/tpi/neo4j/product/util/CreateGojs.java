package com.tpi.neo4j.product.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.tpi.neo4j.product.datamodel.dto.gojs.GoJS;
import com.tpi.neo4j.product.datamodel.dto.gojs.GoLink;
import com.tpi.neo4j.product.datamodel.dto.gojs.GoNode;
import com.tpi.neo4j.product.datamodel.dto.gojs.Linked;

@Component
public class CreateGojs {
  @Autowired private HttpExecutor httpExecutor;
  private static final String PROPERTY = "properties";
  private static final String NODE = "node_";
  private static final String ELEMENT_ID = "elementId";
  private static final String CAP_LABEL = "LABEL";

  public String execute(String query) throws IOException {
    return gojsResult(httpExecutor.httpapi(query));
  }

  public String execute(List<String> listOfQuery) throws IOException {

    List<String> responseFromHttpExecutor = new ArrayList<>();

    for (String eachQuery : listOfQuery) {
      responseFromHttpExecutor.add(httpExecutor.httpapi(eachQuery));
    }

    return gojsResult(responseFromHttpExecutor);
  }

  public static String gojsResult(String query) {
    List<String> qList = new ArrayList<>();
    qList.add(query);

    return gojsResult(qList);
  }

  public static String gojsResult(List<String> listOfQuery) {

    GoJS go = new GoJS();
    List<GoNode> nodeList = new ArrayList<>();
    List<GoLink> linkList = new ArrayList<>();
    Map<String, GoLink> lineMap = new HashMap<>();
    Map<String, GoNode> nodeMap = new HashMap<>();
    List<String> list = new ArrayList<>();

    for (String query : listOfQuery) {
      list.add(query);
    }

    list.forEach(
        json -> {
          JsonObject jsonObject = JsonParser.parseString(json).getAsJsonObject();
          JsonArray data =
              jsonObject
                  .get("results")
                  .getAsJsonArray()
                  .get(0)
                  .getAsJsonObject()
                  .get("data")
                  .getAsJsonArray();
          for (JsonElement g : data) {

            JsonElement jsonElement = g.getAsJsonObject().get("graph");
            JsonArray nodes = jsonElement.getAsJsonObject().get("nodes").getAsJsonArray();
            for (JsonElement n : nodes) {

              String elementId = extractOutString(n, ELEMENT_ID);
              long id = extractOutLong(n, "id");
              JsonArray labels = n.getAsJsonObject().get("labels").getAsJsonArray();

              JsonElement property = getPropertiesFromJsonElement(n, PROPERTY);
              Map<String, String> properties = extractPropertiesIntoMap(property);

              GoNode gNode = new GoNode();
              gNode.setElementId(elementId);
              gNode.setKey(NODE + id);

              if (labels.size() > 1) {
                StringBuilder sb = new StringBuilder();
                for (int k = 0; k < labels.size(); k++) {
                  sb.append(labels.get(k).getAsString());
                }
                gNode.setCategory(sb.toString());
              } else {
                String label = labels.get(0).getAsString();
                gNode.setCategory(label.toUpperCase().replace(",", ""));
              }

              /* 決定 imageType */
              gNode.refreshImageType();
              gNode.setText(labels.get(0).getAsString());
              gNode.setData(properties);
              nodeMap.put(gNode.getKey(), gNode);
            }

            // 組合出 relationship 內容
            JsonArray relationships =
                jsonElement.getAsJsonObject().get("relationships").getAsJsonArray();
            for (JsonElement r : relationships) {
              String elementId = extractOutString(r, ELEMENT_ID);
              long id = extractOutLong(r, "id");
              long startNode = extractOutLong(r, "startNode");
              long endNode = extractOutLong(r, "endNode");
              String type = extractOutString(r, "type");

              JsonElement property = getPropertiesFromJsonElement(r, PROPERTY);
              Map<String, String> properties = extractPropertiesIntoMap(property);

              GoLink goLink = new GoLink();
              goLink.setElementId(elementId);
              goLink.setKey("link_" + id);
              if (properties.get(CAP_LABEL) != null) {
                goLink.setText(properties.get(CAP_LABEL).toString());
              } else {
                goLink.setText(type);
              }
              goLink.setFrom(NODE + startNode);
              goLink.setTo(NODE + endNode);
              goLink.setData(properties);
              goLink.setCurviness(0); // default assign value 0
              lineMap.put(goLink.getKey(), goLink);
            }
          }
        });
    nodeList.addAll(nodeMap.values());
    linkList.addAll(lineMap.values());
    go.setNodeDataArray(nodeList);
    go.setLinkDataArray(linkList);

    Linked link = new Linked();
    List<GoJS> linkedList = new ArrayList<>();
    linkedList.add(go);
    link.setItems(linkedList);

    return new Gson().toJson(link);
  }

  public static JsonElement getPropertiesFromJsonElement(JsonElement r, String properties) {
    return r.getAsJsonObject().get(properties);
  }

  public static Map<String, String> extractPropertiesIntoMap(JsonElement jsonElement) {
    return new Gson().fromJson(jsonElement, new TypeToken<Map<String, String>>() {}.getType());
  }

  public static String extractOutString(JsonElement je, String str) {
    return je.getAsJsonObject().get(str).getAsString();
  }

  public static Long extractOutLong(JsonElement je, String str) {
    return je.getAsJsonObject().get(str).getAsLong();
  }
}
