package com.tpi.neo4j.product.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.stereotype.Repository;

import com.tpi.neo4j.product.datamodel.node.Node;

@Repository
public interface GeneralNodeRepository extends Neo4jRepository<Node, Long> {

  @Query("MATCH (n:`:#{literal(#label)}`) RETURN n")
  List<Node> findByLabel(String label);

  @Query("MATCH (n:`:#{allOf(#label)}`) RETURN n")
  List<Node> findByLabels(List<String> labels);

  @Query("MATCH (n) WHERE id(n) = $id RETURN n")
  Optional<Node> findByNodeId(Long id);

  @Query("MATCH (n) where elementId(n) = $elementId RETURN n")
  Optional<Node> findByElementId(String elementId);

}
