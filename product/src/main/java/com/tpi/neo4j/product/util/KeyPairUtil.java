package com.tpi.neo4j.product.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.crypto.Cipher;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tpi.neo4j.product.exception.HttpException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class KeyPairUtil {

  private ObjectMapper mapper = new ObjectMapper();

  /**
   * 驗證授權資料
   *
   * @param authorizationCode
   * @throws Exception
   */
  public void validCertificate() throws Exception {
    X509Certificate cert = readCertificate();

    validAuthorizationCode(readAuthorizationCode(), cert.getPublicKey());
  }

  /**
   * 取得授權人數上限
   *
   * @param authorizationCode
   * @return
   * @throws Exception
   */
  public Integer getAuthorizationLimitUserCount() throws Exception {

    X509Certificate cert = readCertificate();
    // 驗證授權碼
    JsonNode decryptedData = decrypt(readAuthorizationCode(), cert.getPublicKey());

    JsonNode limitUser = decryptedData.get("LIMIT_USER");
    if (limitUser == null) {
      throw new HttpException("授權資料設定錯誤");
    }
    log.info("授權人數上限:"+ limitUser.asText());
    return Integer.valueOf(limitUser.asText());
  }

  private X509Certificate readCertificate() throws HttpException {
    X509Certificate cert;
    try {
      CertificateFactory cf = CertificateFactory.getInstance("X.509");
      FileInputStream fis = new FileInputStream("neo4jCanvas.crt");
      cert = (X509Certificate) cf.generateCertificate(fis);
    } catch (Exception e) {
      throw new HttpException("讀取憑證失敗");
    }

    return cert;
  }

  /**
   * 解密並且確認驗證
   *
   * @param authorizationCode
   * @param publicKey
   * @throws Exception
   */
  private void validAuthorizationCode(byte[] authorizationCode, PublicKey publicKey)
      throws Exception {
    // 驗證授權碼
    JsonNode decryptedData = decrypt(authorizationCode, publicKey);

    LocalDate today = LocalDate.now();
    LocalDate endDate =
        LocalDate.parse(
            decryptedData.get("END_DATE").textValue(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    if (today.isAfter(endDate)) {
      throw new HttpException("憑證截止日已到期:" + endDate);
    } else {
      log.info("憑證認證成功");
    }
  }

  private JsonNode decrypt(byte[] encryptedData, PublicKey publicKey) throws Exception {
    Cipher cipher = Cipher.getInstance("RSA");
    cipher.init(Cipher.DECRYPT_MODE, publicKey);

    byte[] decryptedData = cipher.doFinal(encryptedData);
    return mapper.readTree(new String(decryptedData, StandardCharsets.UTF_8));
  }

  private byte[] readAuthorizationCode() {
    try {
      return Files.readAllBytes(Paths.get("AuthorizationCode.txt"));
    } catch (IOException e) {
      log.error("讀取AuthorizationCode.txt失敗", e);
    }
    return new byte[0];
  }
}
