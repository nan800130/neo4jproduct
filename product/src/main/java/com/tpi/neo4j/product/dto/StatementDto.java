package com.tpi.neo4j.product.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
public class StatementDto implements Serializable {

  /** */
  private static final long serialVersionUID = 1L;

  private String statement;

  private List<String> resultDataContents;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Map<String, String> parameters;
}
