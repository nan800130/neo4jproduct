package com.tpi.neo4j.product.datamodel.dto.gojs;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class GoJS implements Serializable {
	
  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  public static final String TITLE = "go.GraphLinksModel";
  
  public static final String NODE_KEY_PROPERTY = "key";

  private List<GoNode> nodeDataArray;
  
  private List<GoLink> linkDataArray;
  
}
