package com.tpi.neo4j.product.datamodel.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import java.io.Serial;
import java.io.Serializable;

@Data
public class DemoRequestDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @JsonProperty("QueryString")
    @NotBlank(message = "queryString 不可為空")
    private String queryString;

}
