package com.tpi.neo4j.product.datamodel.node.finance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.neo4j.core.schema.Node;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Node("Account")
public class Account extends com.tpi.neo4j.product.datamodel.node.Node {
    @JsonProperty("account_id")
    private Integer account_id;

    @JsonProperty("date")
    private Integer date;

}
