package com.tpi.neo4j.product.datamodel.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import java.io.Serializable;

@Data
public class Top5RelationshipEntityDto implements Serializable {
  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  @JsonProperty("relationship")
  private String relationship;
}
