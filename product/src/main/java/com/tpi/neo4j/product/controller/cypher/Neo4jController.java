package com.tpi.neo4j.product.controller.cypher;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tpi.neo4j.product.configuration.Neo4jDbConfig;
import com.tpi.neo4j.product.datamodel.dto.request.DemoRequestDto;
import com.tpi.neo4j.product.dto.BaseDataDto;
import com.tpi.neo4j.product.service.RedisService;
import com.tpi.neo4j.product.util.CreateGojs;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api")
public class Neo4jController {

  @Autowired private CreateGojs createGojs;

  @Autowired private RedisService redisService;

  @Value("${logo.image}")
  public String logoImageEncodedInBased64;

  @Autowired private Neo4jDbConfig neo4jDbConfig;

  @PostMapping(value = "/cypher/query", produces = MediaType.APPLICATION_JSON_VALUE)
  public String neo4jQuery(@Valid @RequestBody DemoRequestDto req) throws IOException {

    return createGojs.execute(req.getQueryString());
  }

  @GetMapping("/cypher/test")
  public ResponseEntity<String> test() throws IOException {
    String query = "MATCH (n:Account) RETURN n LIMIT 25";
    return ResponseEntity.ok(createGojs.execute(query));
  }

  @GetMapping("/logo")
  public Map<String, String> logo() {

    Map<String, String> imageBase64String = new HashMap<>();
    imageBase64String.put("data", logoImageEncodedInBased64);

    return imageBase64String;
  }

  @GetMapping("/dblist")
  public ResponseEntity<BaseDataDto<List<String>>> dblist() {
    return ResponseEntity.ok(new BaseDataDto<>(neo4jDbConfig.getDatabaseList()));
  }

  @GetMapping("/dbselect/{dbName}")
  public void selectDb(@PathVariable String dbName) {

    redisService.setSelectDb(dbName);

  }

  @GetMapping("/currentSelectDb")
  public ResponseEntity<BaseDataDto<String>> currentSelectDb() {

    return ResponseEntity.ok(new BaseDataDto<>(redisService.getSelectDb()));
  }
}
