package com.tpi.neo4j.product.datamodel.dto.gojs;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

@Data
public class GoNode implements Serializable {
  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  private String elementId;

  private String key;

  private String text;

  private boolean isVisible = true;

  private String category;

  private String imageType;

  private Map<String, String> data;

  //  public GoNode() {
  //    isVisible = true;
  //  }

  public void refreshImageType() {
    switch (this.category) {
      case "CARD":
        this.imageType = "credit_card";
        break;
      case "ACCOUNT":
        this.imageType = "account_balance";
        break;
      case "DISTRICT":
        this.imageType = "contact_mail";
        break;
      case "LOAN":
        this.imageType = "real_estate_agent";
        break;
      case "TRANSACTION":
        this.imageType = "receipt_long";
        break;
      default:
        this.imageType = null;
    }
  }
}
