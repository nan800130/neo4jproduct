package com.tpi.neo4j.product.datamodel.relationship;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Relationship {
    @Id
    @GeneratedValue
    @JsonProperty("id")
    private Long id;

    @JsonProperty("elementId")
    private String elementId;

    @JsonProperty("startElementId")
    private String startElementId;

    @JsonProperty("endElementId")
    private String endElementId;
    
    @JsonProperty("type")
    private String type;

}
