package com.tpi.neo4j.product.exception;

import java.io.IOException;

public class HttpException extends IOException {
    public HttpException(String message) {
        super(message);
    }
}
