package com.tpi.neo4j.product.configuration;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tpi.neo4j.product.datamodel.dto.response.ResponseDto;
import com.tpi.neo4j.product.filter.JwtAuthenticationFilter;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@EnableWebSecurity
public class SecurityConfig {

  @Autowired private ObjectMapper mapper;

  @Bean
  public InMemoryUserDetailsManager inMemoryUserDetailManager() {

    // 開發測試使用
    UserDetails user1 = User.withUsername("user1").password("111111").build();
    UserDetails user2 = User.withUsername("user2").password("222222").build();
    UserDetails user3 = User.withUsername("user3").password("333333").build();
    UserDetails user4 = User.withUsername("user4").password("444444").build();
    UserDetails user5 = User.withUsername("user5").password("555555").build();

    return new InMemoryUserDetailsManager(List.of(user1, user2, user3, user4, user5));
  }

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

    http.authorizeHttpRequests(
            authHttp ->
                authHttp
                    .requestMatchers("/api/auth/**", "/api/logo")
                    .permitAll()
                    .requestMatchers(
                        "/swagger-ui/**",
                        "/api-docs/**",
                        "/configuration/ui",
                        "/configuration/security",
                        "/swagger-resources/**",
                        "/webjars/**")
                    .permitAll()
                    .anyRequest()
                    .authenticated())
        .sessionManagement(
            session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
        .addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
        .authenticationManager(authenticationManager())
        .csrf(csrf -> csrf.disable())
        .exceptionHandling(
            eh -> eh.authenticationEntryPoint(this::handler).accessDeniedHandler(this::handler))
        .logout(logout -> logout.permitAll());

    return http.build();
  }

  @Bean
  PasswordEncoder passwordEncoder() {
    return NoOpPasswordEncoder.getInstance();
  }

  @Bean
  public AuthenticationManager authenticationManager() {
    return new ProviderManager(daoAuthenticationProvider());
    // 若有多個驗證方式，可使用：
    // List.of(daoAuthenticationProvider(), customAuthenticationProvider())
  }

  @Bean
  public DaoAuthenticationProvider daoAuthenticationProvider() {
    DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
    authenticationProvider.setUserDetailsService(inMemoryUserDetailManager());
    authenticationProvider.setPasswordEncoder(passwordEncoder());
    return authenticationProvider;
  }

  @Bean
  public JwtAuthenticationFilter jwtAuthenticationFilter() {
    return new JwtAuthenticationFilter();
  }

  private void handler(HttpServletRequest req, HttpServletResponse resp, Exception e)
      throws IOException {
    log.error(e.getMessage());
    resp.setContentType("application/json;charset=utf-8");
    resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
    //    PrintWriter out = resp.getWriter();
    //    out.write(mapper.writeValueAsString(ResponseDto.failare(e.getMessage())));
    //    out.flush();
    //    out.close();
  }
}
