package com.tpi.neo4j.product.datamodel.dto.gojs;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

@Data
public class GoLink implements Serializable {
  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  private String elementId;
  private String key;
  private String from;
  private String to;
  private String text;

  private boolean isVisible = true;
  private String category;
  private Map<String, String> data;
  private int curviness = 0;
}
