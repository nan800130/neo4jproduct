package com.tpi.neo4j.product.datamodel.node.finance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.neo4j.core.schema.Node;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Node("Loan")
public class Loan extends com.tpi.neo4j.product.datamodel.node.Node {
    @JsonProperty("loan_id")
    private Integer loan_id;
    @JsonProperty("account_id")
    private Integer account_id;
    @JsonProperty("date")
    private Integer date;
    @JsonProperty("duration")
    private Integer duration;

    @JsonProperty("amount")
    private Integer amount;

    @JsonProperty("payments")
    private Double payments;

    @JsonProperty("status")
    private String status;

}
