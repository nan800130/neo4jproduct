package com.tpi.neo4j.product.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

import com.tpi.neo4j.product.service.RedisService;

@Component
public class Neo4jAppListener implements ApplicationListener<ContextClosedEvent> {

  @Autowired private RedisService redisService;

  @Override
  public void onApplicationEvent(ContextClosedEvent event) {
    redisService.clearCache();
  }
}
