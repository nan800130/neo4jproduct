package com.tpi.neo4j.product.controller.cypher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tpi.neo4j.product.datamodel.dto.request.ElementIdRequestDto;
import com.tpi.neo4j.product.datamodel.dto.request.NodeLabelRequestDto;
import com.tpi.neo4j.product.datamodel.node.Node;
import com.tpi.neo4j.product.exception.FetchNodeException;
import com.tpi.neo4j.product.exception.InvalidElementIdException;
import com.tpi.neo4j.product.exception.InvalidNodeLabelException;
import com.tpi.neo4j.product.exception.NotFoundNodeException;
import com.tpi.neo4j.product.service.GeneralNodeService;
import com.tpi.neo4j.product.util.CreateGojs;
import com.tpi.neo4j.product.util.HttpExecutor;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/node")
public class NodeController {
  @Autowired private GeneralNodeService generalNodeService;

  @Autowired private HttpExecutor httpExecutor;

  @Autowired private CreateGojs createGojs;

  @GetMapping("/{label}/{nodeId}")
  public String fetchSingleNodeById(@PathVariable String label, @PathVariable String nodeId)
      throws FetchNodeException {
    String regex = "node_\\d+";

    /* parse the node_id from request body as a node id with certain label */
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(nodeId);
    String result = "";
    if (matcher.find()) {
      result = matcher.group(1);

    } else {

      throw new FetchNodeException("No match found.");
    }

    /* check the node label exist or not,
     * if so, then search with label,
     * otherwise, throw an exception error
     * */

    return "label : " + label + " ; id : " + result;
  }

  @GetMapping("/{nodeLabel}")
  public List<Node> getNodesByLabel(@PathVariable String nodeLabel)
      throws InvalidNodeLabelException, FetchNodeException {

    if (nodeLabel == null || nodeLabel.isEmpty()) {
      throw new InvalidNodeLabelException(nodeLabel);
    } else {
      List<Node> nodeList = generalNodeService.getNodesByLabel(nodeLabel);
      if (nodeList.isEmpty()) {
        throw new NotFoundNodeException("there is empty list returned in getNodesByLabel");
      } else {
        return nodeList;
      }
    }
  }

  @GetMapping("/detail/{nodeId}")
  public Node getNodesById(@PathVariable Long nodeId) throws FetchNodeException {
    Node node = generalNodeService.getNodesById(nodeId);
    if (node != null) {
      return node;
    } else {
      throw new FetchNodeException("No any node found.");
    }
  }

  @PostMapping("/detail")
  public Node getNodesByElementId(@Valid @RequestBody ElementIdRequestDto elementIdRequestDto)
      throws InvalidElementIdException, FetchNodeException {
    String elementId = elementIdRequestDto.getElementId();
    Node node = generalNodeService.getNodesByElementId(elementId);

    if (elementId != null) {
      if (node != null) {
        return node;
      } else {
        throw new FetchNodeException("No Node found in getNodesByElementId function");
      }
    } else {
      throw new InvalidElementIdException(elementId);
    }
  }

  @GetMapping("/distinct_node_labels")
  public Map<String, List<String>> returnDistinctNodeLabels() throws IOException {
    String query = "call db.labels()";
    return generalNodeService.parseDistinctNodeLabels(httpExecutor.httpapi(query));
  }

  @PostMapping()
  public String fetchNodesByLabel(@Valid @RequestBody NodeLabelRequestDto nodeLabelRequestDto)
      throws IOException {

    // iterates the filter list, then fetch node label from the list
    List<String> labels = nodeLabelRequestDto.getNodeLabels();
    String selectedLabels = "";
    StringBuilder strbld = new StringBuilder();

    Integer limit = nodeLabelRequestDto.getLimit();

    if (labels.isEmpty()) {
      // 前端傳遞空陣列表示全部 relationship type 都要
      strbld.append("n");
    } else if (!labels.isEmpty()) {
      /* 將複數選項的字串取出來，再以字串連接作為WHERE條件篩選 */
      for (String label : labels) {
        if (labels.indexOf(label) == 0) {
          strbld.append("n:" + label);
        } else {
          strbld.append("|" + label);
        }
      }
    } else {
      // error handle with size < 0
      log.info("ERROR : labels list size < 0 !!");
    }

    log.info("where condition : " + strbld.toString());
    selectedLabels = strbld.toString();
    String matchClause = "MATCH (" + selectedLabels + ") ";

    String returnClause = "RETURN n ";
    String limitClause = "LIMIT " + limit;
    String concatQuery = matchClause + returnClause + limitClause;

    return createGojs.execute(concatQuery);
  }

  @PostMapping("/multiple")
  public String fetchMultipleNodesByLabel(
      @Valid @RequestBody NodeLabelRequestDto nodeLabelRequestDto) throws IOException {

    // iterates the filter list, then fetch node label from the list
    List<String> labels = nodeLabelRequestDto.getNodeLabels();
    Integer limit = nodeLabelRequestDto.getLimit();

    int totalSize = labels.size(); // 複選中勾選了多少個相異 node label 個數
    int eachSize =
        (limit / totalSize) == 0
            ? (limit / 10) + 1
            : (limit / totalSize); // 每個 node label 平均有多少數量需要被回傳，取floor

    String matchClause = "MATCH p=(q:";
    String returnClause = ") RETURN p LIMIT ";
    String finalQuery = "";

    log.info("given limit : %d\nsize of total : %d\neach size : %d", limit, totalSize, eachSize);

    List<String> multipleResults = new ArrayList<>();

    for (String eachNodeLabel : labels) {
      finalQuery = matchClause + eachNodeLabel + returnClause + eachSize;
      multipleResults.add(finalQuery);
    }

    return createGojs.execute(multipleResults);
  }

}
