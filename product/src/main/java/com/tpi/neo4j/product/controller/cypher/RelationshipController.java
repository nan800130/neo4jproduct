package com.tpi.neo4j.product.controller.cypher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tpi.neo4j.product.datamodel.dto.request.ElementIdRequestDto;
import com.tpi.neo4j.product.datamodel.dto.request.RelationshipTypesRequestDto;
import com.tpi.neo4j.product.datamodel.dto.request.SearchKeyStrRequestDto;
import com.tpi.neo4j.product.datamodel.dto.request.Top5RelationshipEntityDto;
import com.tpi.neo4j.product.datamodel.relationship.Relationship;
import com.tpi.neo4j.product.exception.InvalidElementIdException;
import com.tpi.neo4j.product.exception.NotFoundRelationshipException;
import com.tpi.neo4j.product.repository.GeneralRelationshipRepository;
import com.tpi.neo4j.product.service.GeneralRelationshipService;
import com.tpi.neo4j.product.util.CreateGojs;
import com.tpi.neo4j.product.util.HttpExecutor;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/relationship")
public class RelationshipController {

    @Autowired
    private GeneralRelationshipService generalRelationshipService;

    @Autowired
    private GeneralRelationshipRepository generalRelationshipRepository;

    @Autowired
    private HttpExecutor httpExecutor;
    
    @Autowired
    private CreateGojs createGojs;
    
    @GetMapping("/{type}/{relId}")
    public String fetchSingleRelationshipById(@PathVariable String type, @PathVariable String relId) throws NotFoundRelationshipException {
        String regex = "link_\\d+";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(relId);
        String result="";
        if (matcher.find()) {
            result = matcher.group(1);
        } else {
            throw new NotFoundRelationshipException("No any match relationship found.");
        }
        return "rel type : "+type+" ; id : "+result;
    }

    @GetMapping("/distinct_relationship")
    public Map<String, List<String>> returnDistinctRelationshipLabels() throws IOException {
        String query = "CALL db.relationshipTypes()";
        return generalRelationshipService.parseDistinctRelationshipTypes(httpExecutor.httpapi(query));
    }

    @PostMapping("/connected_relationships")
    public Map<String,List<String>> connectedRelationships(@RequestBody ElementIdRequestDto elementIdRequestDto) throws NotFoundRelationshipException, InvalidElementIdException {
        String elementId = elementIdRequestDto.getElementId();
        Map<String,List<String>> connRelationships = null;
        if(elementId != null){
            try{
                connRelationships = generalRelationshipService.connectedRelationships(elementId);
                throw new NotFoundRelationshipException("connRelationships is empty");
            }
            catch (NotFoundRelationshipException ex){
                ex.setErrorMessage("Not Found Relationship Exception occurs.");
            }
        }else {
            throw new InvalidElementIdException(elementId);
        }
        return connRelationships;
    }

    @PostMapping("/top5connected")
    public Map<String, List<Top5RelationshipEntityDto>> top5ConnectedRelationships(@RequestBody ElementIdRequestDto elementIdRequestDto) throws InvalidElementIdException {
        String elementId = elementIdRequestDto.getElementId();
        if(elementId == null) {throw new InvalidElementIdException(elementId);}

        return generalRelationshipService.top5connectedRelationships(elementId);
    }

    @PostMapping("/containsString")
    public Map<String,List<String>> containsString(@RequestBody SearchKeyStrRequestDto searchKeyStrRequestDto) throws InvalidElementIdException {
        String searchKeyStr = searchKeyStrRequestDto.getSearchedKey();
        String elementId = searchKeyStrRequestDto.getElementId();

        if(elementId == null){ throw new InvalidElementIdException(elementId);}
        return generalRelationshipService.containsString(elementId, searchKeyStr);
    }

    @PostMapping()
    public String fetchRelationshipsByType(@Valid @RequestBody RelationshipTypesRequestDto relationshipTypesRequestDto) throws IOException {

        //iterates the filter list, then fetch node label from the list
        List<String> types = relationshipTypesRequestDto.getRelTypes();
        String selectedTypes = "";
        StringBuilder strbld = new StringBuilder();
        Integer limit = relationshipTypesRequestDto.getLimit();  //前端要求總共要有多少數量

        if(types.isEmpty()){
            //前端傳遞空陣列表示全部 relationship type 都要
            strbld.append("r");
        }
        else if(!types.isEmpty()) {
            /* 將複數選項的字串取出來，再以字串連接作為WHERE條件篩選 */
            for (String type : types) {
                if (types.indexOf(type) == 0) {

                    strbld.append(":"+type);
                } else {

                    strbld.append("|"+type);
                }
            }
        }else{
            //error handle with size < 0
            log.info("ERROR : types list size < 0 !!");
        }

        log.info("selectedTypes : "+strbld.toString());
        selectedTypes = strbld.toString();

        String fromClause = "MATCH p=()-";
        String relationClause = "["+selectedTypes+"]";
        String toClause = "-() ";
        String whereClause = "";
        String returnClause = "RETURN p ";
        String limitClause = "LIMIT "+limit;
        String concatQuery = fromClause+relationClause+toClause+whereClause+returnClause+limitClause;

        return createGojs.execute(concatQuery);
    }

    @PostMapping("/multiple")
    public String fetchMultipleRelationshipsByType(@Valid @RequestBody RelationshipTypesRequestDto relationshipTypesRequestDto) throws IOException {

        //iterates the filter list, then fetch node label from the list
        List<String> types = relationshipTypesRequestDto.getRelTypes();
        Integer limit = relationshipTypesRequestDto.getLimit();  //前端要求總共要有多少數量
        int totalSize = types.size(); //複選中勾選了多少個相異 relationship 個數
        int eachSize = (limit/totalSize)==0? (limit/10)+1 : (limit/totalSize); //每個 relationship 平均有多少數量需要被回傳，取floor

        String matchClause = "MATCH p=()-[r:";
        String returnClause = "]-() RETURN p LIMIT ";
        String finalQuery = "";

        log.info("given limit : %d\nsize of total : %d\neach size : %d",limit,totalSize,eachSize);

        List<String> multipleResults = new ArrayList<>();

        for(String eachRelationshipType : types) {
            finalQuery = matchClause + eachRelationshipType + returnClause + eachSize;
            multipleResults.add(finalQuery);
        }

        return createGojs.execute(multipleResults);
    }


    @PostMapping("/detail")
    public Relationship getRelationshipDetailByElementId(@Valid @RequestBody ElementIdRequestDto elementIdRequestDto) throws InvalidElementIdException {

        String elementId = elementIdRequestDto.getElementId();
        if(elementId == null){throw new InvalidElementIdException(elementId);}

        Optional<Object> optionalNode = generalRelationshipRepository.findByElementId(elementId);
        Object rel = optionalNode.isPresent()? optionalNode.get():null;
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        String jsonString = gson.toJson(rel);

        JsonObject jsonObject = JsonParser.parseString(jsonString).getAsJsonObject();
        jsonObject = jsonObject.get("adapted").getAsJsonObject();

        return gson.fromJson(jsonObject.toString(),Relationship.class);

    }

}
