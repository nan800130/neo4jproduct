package com.tpi.neo4j.product.datamodel.dto.db;

import java.io.Serializable;

import lombok.Data;

@Data
public class Neo4jDbSetting implements Serializable {

  /** serialVersionUID */
  private static final long serialVersionUID = 1L;

  private String database;

  private String username;

  private String credential;

  private String url;

  private static final String URL_COMMIT = "/tx/commit";

  public String getCommitUrl() {

    return url + database + URL_COMMIT;
  }
}
