package com.tpi.neo4j.product.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tpi.neo4j.product.configuration.Neo4jDbConfig;
import com.tpi.neo4j.product.datamodel.dto.db.Neo4jDbSetting;
import com.tpi.neo4j.product.dto.Neo4jApiErrorsDto;
import com.tpi.neo4j.product.dto.PayloadDto;
import com.tpi.neo4j.product.dto.StatementDto;
import com.tpi.neo4j.product.exception.HttpException;
import com.tpi.neo4j.product.service.RedisService;

import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class HttpExecutor {

  @Autowired private Neo4jDbConfig neo4jDbConfig;

  @Autowired private RedisService redisService;

  @Autowired private ObjectMapper mapper;

  private static final String ERROR_MSG = "Neo4j API has wrong: ";

  /**
   * @param statement Cypher查詢子句
   * @return String 查詢的資料(JsonString)
   * @throws IOException
   */
  public String httpapi(String statement) throws IOException {
    return httpapi(statement, "");
  }

  /**
   * @param statement Cypher查詢子句
   * @param neo4jDbName 該user選擇查詢的DB(沒傳就使用預設DB)
   * @return String 查詢的資料(JsonString)
   * @throws IOException
   */
  public String httpapi(String statement, String neo4jDbName) throws IOException {

    String[] statements = {statement};
    return httpapi(statements, neo4jDbName);
  }

  /**
   * @param statements Cypher查詢子句(多個語法)
   * @return String 查詢的資料(JsonString)
   * @throws IOException
   */
  public String httpapi(String[] statements) throws IOException {
    return httpapi(statements, null);
  }

  /**
   * @param statements Cypher查詢子句(多個語法)
   * @param neo4jDbName 該user選擇查詢的DB(沒傳就使用預設DB)
   * @return String 查詢的資料(JsonString)
   * @throws IOException
   */
  public String httpapi(String[] statements, String neo4jDbName) throws IOException {

    if (StringUtils.isBlank(neo4jDbName)) {
      neo4jDbName = redisService.getSelectDb();
    }

    Neo4jDbSetting dbSetting = neo4jDbConfig.getDbSetting(neo4jDbName);
    String commitUrl = dbSetting.getCommitUrl();
    String database = dbSetting.getDatabase();

    log.info("database : " + database + "; uri : " + commitUrl);
    String rawData = payload(statements);

    return httpResponse(rawData, neo4jDbName);
  }

  /**
   * @param statement Cypher查詢子句
   * @param params Cypher語法須帶入的參數
   * @return String 查詢的資料(JsonString)
   * @throws IOException
   */
  public String httpapi(String statement, Map<String, String> params) throws IOException {

    return httpapi(statement, params, null);
  }

  /**
   * @param statement Cypher查詢子句
   * @param params Cypher語法須帶入的參數
   * @param neo4jDbName 該user選擇查詢的DB(沒傳就使用預設DB)
   * @return String 查詢的資料(JsonString)
   * @throws IOException
   */
  public String httpapi(String statement, Map<String, String> params, String neo4jDbName)
      throws IOException {

    String rawData = payload(statement, params);

    return httpResponse(rawData, neo4jDbName);
  }

  private String httpResponse(String rawData, String neo4jDbName) throws IOException {

    if (StringUtils.isBlank(neo4jDbName)) {
      neo4jDbName = redisService.getSelectDb();
    }
    Neo4jDbSetting dbSetting = neo4jDbConfig.getDbSetting(neo4jDbName);
    String username = dbSetting.getUsername();
    String credential = dbSetting.getCredential();
    String commitUrl = dbSetting.getCommitUrl();
    URL url = new URL(commitUrl);
    URLConnection conn = url.openConnection();
    HttpURLConnection http = (HttpURLConnection) conn;
    http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
    http.setRequestProperty("Accept", "application/json");
    http.setRequestProperty("X-Stream", "true");
    http.setRequestMethod("POST");
    http.setConnectTimeout(600000);
    http.setReadTimeout(600000);
    http.setDoInput(true); // 允許輸入流，即允許下載
    http.setDoOutput(true); // 允許輸出流，即允許上傳
    http.setUseCaches(false); // 設置是否使用緩存

    String userpass = username + ":" + credential;
    String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
    http.setRequestProperty("Authorization", basicAuth);

    byte[] out = rawData.getBytes();
    http.connect();
    try (OutputStream os = http.getOutputStream()) {
      os.write(out);
    }
    int code = http.getResponseCode();
    if (code != 200) {

      log.error("[neo4j Api]-->ReturnCode:{}, ReturnMsg:{}", code, http.getResponseMessage());
      throw new HttpException(ERROR_MSG + code + ":" + http.getResponseMessage());
    }
    StringBuilder response = new StringBuilder();
    try (BufferedReader br =
        new BufferedReader(new InputStreamReader(http.getInputStream(), StandardCharsets.UTF_8))) {
      String responseLine = null;
      while ((responseLine = br.readLine()) != null) {
        response.append(responseLine.trim());
      }
    }

    // response code回200, 不代表就是沒錯誤
    String strResp = response.toString();
    Neo4jApiErrorsDto errors = mapper.readValue(strResp, Neo4jApiErrorsDto.class);
    if (!Collections.isEmpty(errors.getErrors())) {
      String errorCode = errors.getErrors().get(0).getErrorCode();
      throw new HttpException("{\"error\":\"" + errorCode + "\"}");
    }

    return strResp;
  }

  private String payload(String[] statements) {

    PayloadDto payload = new PayloadDto();
    List<String> dataContents = Arrays.asList("row", "graph");

    List<StatementDto> statementList = new ArrayList<>();
    for (String statement : statements) {
      StatementDto statementDto = new StatementDto();
      statementDto.setStatement(statement);
      statementDto.setResultDataContents(dataContents);
      statementList.add(statementDto);
    }
    payload.setStatements(statementList);
    String jsonString = "";
    try {
      jsonString = mapper.writeValueAsString(payload);

      log.info(
          "@@--->>payload Jackson:"
              + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(payload));
    } catch (JsonProcessingException e) {

      log.error("", e);
    }

    return jsonString;
  }

  private String payload(String statement, Map<String, String> ht) {

    PayloadDto payload = new PayloadDto();

    List<String> dataContents = Arrays.asList("row", "graph");

    List<StatementDto> statementList = new ArrayList<>();
    StatementDto statementDto = new StatementDto();
    statementDto.setStatement(statement);
    statementDto.setResultDataContents(dataContents);
    statementDto.setParameters(ht);
    statementList.add(statementDto);
    payload.setStatements(statementList);
    String jsonString = "";
    try {
      jsonString = mapper.writeValueAsString(payload);

      log.info(
          "@@--->>payload Jackson:"
              + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(payload));
    } catch (JsonProcessingException e) {

      log.error("", e);
    }

    return jsonString;
  }
}
