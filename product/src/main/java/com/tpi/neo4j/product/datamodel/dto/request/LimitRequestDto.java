package com.tpi.neo4j.product.datamodel.dto.request;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;

// Dto object with basic validation
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class LimitRequestDto {

        @Positive
        @NotNull
        @Range(min = 10, max = 1000)
        private Integer limit;

}