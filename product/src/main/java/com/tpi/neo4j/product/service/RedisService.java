package com.tpi.neo4j.product.service;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.tpi.neo4j.product.exception.AuthHttpException;
import com.tpi.neo4j.product.exception.HttpException;
import com.tpi.neo4j.product.util.KeyPairUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RedisService {

  @Autowired private RedisCacheManager redisCacheManager;

  @Autowired private RedisTemplate<String, Object> redisTemplate;

  @Autowired private KeyPairUtil keyPairUtil;

  public static final String STR_LOGIN_COUNT = "loginCount";

  public static final String STR_LOGIN_TOKEN = "LoginToken";

  public static final String STR_SYSTEM_COUNT = "SystemCount";

  public static final String STR_SELECT_DB = "SelectDb";

  /**
   * 判斷是否重複登錄
   *
   * @param username
   * @return
   * @throws HttpException
   */
  public void checkDuplicateLogin(String username){

    Cache cache = redisCacheManager.getCache(STR_LOGIN_TOKEN);
    if (cache != null && cache.get(username) != null) {
      throw new AuthHttpException("此帳號已登錄或其他地方登錄");
    }
  }

  /**
   * 判斷登入人數是否超過授權數量
   *
   * @throws Exception
   */
  public void checkAuthLimit() throws Exception {
    Cache countCache = redisCacheManager.getCache(STR_SYSTEM_COUNT);
    if (countCache != null) {

      String loginCount = countCache.get(STR_LOGIN_COUNT, String.class);

      if (loginCount == null) {
        countCache.put(STR_LOGIN_COUNT, "1");
      } else {
        Integer iCount = Integer.valueOf(loginCount);

        // 避免token逾期消失, 導致與實際登入人數不同
        Set<String> loginSet = redisTemplate.keys(STR_LOGIN_TOKEN + "::*");
        if (loginSet != null) {
          int loginUserCount = loginSet.size();
          if (loginUserCount > 0 && iCount.intValue() != loginUserCount) {
            iCount = loginUserCount;
          }
        }

        iCount++;
        if (iCount > keyPairUtil.getAuthorizationLimitUserCount()) {
          throw new AuthHttpException("已超過授權人數上限");
        } else {
          countCache.put(STR_LOGIN_COUNT, iCount.toString());
        }
      }
    }
  }

  public void setSelectDb(String dbName) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    Cache dbCache = redisCacheManager.getCache(STR_SELECT_DB);
    if (dbCache != null) {
      dbCache.put(authentication.getName(), dbName);
    }
  }

  /**
   * 取得當前user所選的neo4j db
   *
   * @return
   */
  public String getSelectDb() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    Cache dbCache = redisCacheManager.getCache(STR_SELECT_DB);
    String dbName = "";
    if (dbCache != null) {
      ValueWrapper valDb = dbCache.get(authentication.getName());
      if (valDb != null) {

        dbName = (String) valDb.get();
      }
    }

    return StringUtils.isBlank(dbName) ? "lpetrocelliczech" : dbName;
  }

  /**
   * 移除快取的token
   *
   * @param username
   */
  public void removeToken(String username) {
    Cache cache = redisCacheManager.getCache(STR_LOGIN_TOKEN);
    if (cache != null && cache.get(username) != null) {

      // 避免token逾期消失, 導致與實際登入人數不同
      Set<String> loginSet = redisTemplate.keys(STR_LOGIN_TOKEN + "::*");
      
      cache.evict(username);
      log.info("已移除token:" + username);

      Cache countCache = redisCacheManager.getCache(STR_SYSTEM_COUNT);
      if (countCache != null) {
        ValueWrapper loginCount = countCache.get(STR_LOGIN_COUNT);
        if (loginCount == null) {
          countCache.put(STR_LOGIN_COUNT, "0");
        } else {
          Integer iCount = Integer.valueOf((String) loginCount.get());

          // 避免token逾期消失, 導致與實際登入人數不同
          if (loginSet != null) {
            int loginUserCount = loginSet.size();
            if (loginUserCount > 0 && iCount.intValue() != loginUserCount) {
              iCount = loginUserCount;
            }
          }

          iCount--;

          countCache.put(STR_LOGIN_COUNT, iCount.toString());
        }
      }
    }
  }

  /** 清除所有快取資料 */
  public void clearCache() {
    Cache tokenCache = redisCacheManager.getCache(STR_LOGIN_TOKEN);
    if (tokenCache != null) {
      tokenCache.clear();
    }
    Cache countCache = redisCacheManager.getCache(STR_SYSTEM_COUNT);
    if (countCache != null) {
      countCache.clear();
    }
    Cache dbCache = redisCacheManager.getCache(STR_SELECT_DB);
    if (dbCache != null) {
      dbCache.clear();
    }

    log.info("Redis快取資料已清除");
  }
}
