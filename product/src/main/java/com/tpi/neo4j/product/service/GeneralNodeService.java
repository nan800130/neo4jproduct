package com.tpi.neo4j.product.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tpi.neo4j.product.datamodel.node.Node;
import com.tpi.neo4j.product.repository.GeneralNodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GeneralNodeService {

  @Autowired private GeneralNodeRepository generalNodeRepository;

  public List<Node> getNodesByLabel(String label) {
    return generalNodeRepository.findByLabel(label);
  }

  public Node getNodesById(Long nodeId) {
    Optional<Node> optionalNode = generalNodeRepository.findByNodeId(nodeId);
    return optionalNode.isPresent() ? optionalNode.get() : null;
  }

  public Node getNodesByElementId(String elementId) {
    Optional<Node> optionalNode = generalNodeRepository.findByElementId(elementId);
    return optionalNode.isPresent() ? optionalNode.get() : null;
  }

  public Map<String, List<String>> parseDistinctNodeLabels(String str) {

    JsonObject jsonObject = JsonParser.parseString(str).getAsJsonObject();

    JsonArray data =
        jsonObject
            .get("results")
            .getAsJsonArray()
            .get(0)
            .getAsJsonObject()
            .get("data")
            .getAsJsonArray();

    List<String> distinctNodeLabelList = new ArrayList<>();

    for (JsonElement element : data) {
      String elementValue =
          element.getAsJsonObject().get("row").getAsJsonArray().get(0).getAsString();
      distinctNodeLabelList.add(elementValue);
    }

    Map<String, List<String>> distinctNodeLabels = new HashMap<>();
    distinctNodeLabels.put("distinctNodeLabel", distinctNodeLabelList);

    return distinctNodeLabels;
  }
}
