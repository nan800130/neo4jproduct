package com.tpi.neo4j.product.datamodel.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;


// Dto object with basic validation
@Data
@Builder
@Slf4j
public class LoginRequestDto {
        @NotBlank(message = "Name cannot be blank")
        private String username;

        @NotBlank(message = "Password cannot be blank")
        @Size(min = 6, max = 20, message = "Password must be between 6 and 20 characters")
        private String password;

}