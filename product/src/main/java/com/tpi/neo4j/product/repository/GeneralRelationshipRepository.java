package com.tpi.neo4j.product.repository;

import com.tpi.neo4j.product.datamodel.dto.request.Top5RelationshipEntityDto;
import com.tpi.neo4j.product.datamodel.relationship.Relationship;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface GeneralRelationshipRepository extends Neo4jRepository<Relationship, Long> {

    @Query("MATCH ()-[r]-() where elementId(r) = $elementId RETURN DISTINCT r")
    Optional<Object> findByElementId(String elementId);

    @Query("MATCH (n)-[r]-(m) WHERE elementId(n) = $elementId RETURN DISTINCT type(r)")
    List<String> connectedRelationships(String elementId);

    @Query( "MATCH (n)-[r]-(m) " +
            "WHERE elementId(n) = $elementId " +
            "WITH type(r) AS relationship, COUNT(r) AS COUNT " +
            "ORDER BY COUNT DESC " +
            "LIMIT 5 "+
            "RETURN relationship")
    List<Top5RelationshipEntityDto> top5ConnectedRelationships(String elementId);

    @Query( "MATCH (p)-[r]-() WHERE toLower(toString(type(r))) contains toLower($searchKeyStr) " +
            " and (elementId(p) = $elementId) " +
            " RETURN distinct type(r)")
    List<String> containsString(String elementId,String searchKeyStr);

    @Query(" MATCH ()-[r]-() RETURN DISTINCT type(r) ")
    List<String> findAllRelationshipTypes();
}
