package com.tpi.neo4j.product.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class Neo4jApiErrorsDto implements Serializable {

  /** serialVersionUID */
  private static final long serialVersionUID = 1L;
  
  private Object results;
  
  private List<ErrorResponse> errors;
}
