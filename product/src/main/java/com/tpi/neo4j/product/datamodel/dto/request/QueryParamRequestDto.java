package com.tpi.neo4j.product.datamodel.dto.request;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

// Dto object with basic validation
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class QueryParamRequestDto {

        @Positive
        @Max(value = 1000, message= " 搜尋節點/關係數量最多為 1000")
        private Integer limit;

}