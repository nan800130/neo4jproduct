package com.tpi.neo4j.product.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class PayloadDto implements Serializable {

  /** serialVersionUID */
  private static final long serialVersionUID = 1L;
  
  private List<StatementDto> statements;
}
