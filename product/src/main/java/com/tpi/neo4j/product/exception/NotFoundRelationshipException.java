package com.tpi.neo4j.product.exception;

public class NotFoundRelationshipException extends FetchRelationshipException {

    public NotFoundRelationshipException(String message) {
        super(message);
    }
    public void setErrorMessage(String errMsg){
        this.errorMessage = errMsg;
    }
}
