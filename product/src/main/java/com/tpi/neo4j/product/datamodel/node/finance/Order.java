package com.tpi.neo4j.product.datamodel.node.finance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.neo4j.core.schema.Node;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Node("Order")
public class Order extends com.tpi.neo4j.product.datamodel.node.Node {

    @JsonProperty("account_id")
    private Integer account_id;
    @JsonProperty("order_id")
    private Integer order_id;
    @JsonProperty("bank_to")
    private String bank_to;
    @JsonProperty("k_symbol")
    private String k_symbol;
    @JsonProperty("account_to")
    private String account_to;
    @JsonProperty("amount")
    private Double amount;

}
