### profile activation
1. 路徑：/resource/
新增 application-{customize_name}.properties

2. 在 application.properties 內決定啟用哪一個 profile

```txt
spring.profiles.active={specified_profile}
```

3. 啟動後看到 Spring Boot message "The following 1 profile is active: \"{specified_profile}\" "

### security 

### swagger 
0. 參考網頁：https://hackmd.io/@_XeDPt_GRpu82pxm_rKHrQ/rysozEsCo 
1. pom.xml 檔案需要加入以下 dependency
    ```xml 
        <dependency>
			<groupId>org.springdoc</groupId>
			<artifactId>springdoc-openapi-starter-common</artifactId>
			<version>2.5.0</version>
		</dependency>

        <dependency>
			<groupId>org.springdoc</groupId>
			<artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
			<version>2.5.0</version>
		</dependency>

    ```
2. application.properties 檔案中需要加入
    
    ```txt
    # swagger/openapi
    # springdoc.swagger-ui.enabled=true
    # swagger-ui custom path
    springdoc.swagger-ui.path=/swagger-ui.html
    # /api-docs endpoint custom path
    springdoc.api-docs.path=/api-docs
    ```

3. 確認 securityConfig.class 是否有阻擋 swagger 相關路徑
4. 訪問 http://localhost:8081/swagger-ui/index.html#/ 


